package com.example.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import dagger.Component
import dagger.Module
import dagger.Provides
import javax.inject.Inject
import javax.inject.Qualifier

const val LOVE = "Love"
const val HELLO = "Hello"

class MainActivity : AppCompatActivity() {

    @Inject
    @field:Choose(LOVE) lateinit var infoLove: Info
    @Inject @field:Choose(HELLO) lateinit var infoHello: Info

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        DaggerMagicBox.create().poke(this)
        var text : String = "${infoLove.text} ${infoHello.text}"
        Toast.makeText(this, text,Toast.LENGTH_LONG).show()

        val button:Button = findViewById(R.id.button);
        button.setOnClickListener{
            startActivity(Intent(applicationContext, DemoActivity::class.java))
        }
    }
}

@Module
open class Bag {
    @Provides
    @Choose(LOVE)
    fun sayLoveDagger2(): Info {
        return Info("I Love You")
    }
    @Provides @Choose(HELLO)
    fun sayHelloDagger2(): Info {
        return Info("Hello Dagger 2")
    }
}

class Info(val text: String)

@Component(modules = [Bag::class])
interface MagicBox {
    fun poke(app: MainActivity)
}

@Qualifier
@MustBeDocumented
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class Choose(val value: String = "")