package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import dagger.Component
import kotlinx.android.synthetic.main.activity_demo.*
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject
import javax.inject.Scope

class DemoActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_demo)
        var newMagicBox = DaggerAnotherMagicBox.create()

        counter_button.setOnClickListener {
            val storage = Storage()
            newMagicBox.againPoke(storage)
            counter_button.setText("Unique ${storage.uniqueMagic.count}" + "\nNormal ${storage.normalMagic.count} ")
        }
    }

}


@MagicScope
@Component
interface AnotherMagicBox{
    fun againPoke(storage: Storage)
}


class Storage{
    @Inject lateinit var uniqueMagic: UniqueMagic
    @Inject lateinit var  normalMagic: NormalMagic
}


@Scope
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class MagicScope

var staticCounter = 0

@MagicScope
class UniqueMagic @Inject constructor() {
    val count = staticCounter++
}

class NormalMagic @Inject constructor() {
    val count = staticCounter++
}